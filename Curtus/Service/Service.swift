//
//  Service.swift
//  Curtus
//
//  Created by Vasily Popov on 15/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import Foundation

final class Service {
    
    static var tableTestDataSourceProvider : [String] = {
        return Array(1...100).map { String("Test Data # \($0)") }
    }()
    
    static var collectionTestDataSourceProvider : [CountTimer] = {
        return Array(0...50).map { _ in CountTimer() }
    }()
}
