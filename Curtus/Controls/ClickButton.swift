//
//  ClickButton.swift
//  Curtus
//
//  Created by Vasily Popov on 12/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

@IBDesignable final class ClickButton : UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var background: UIColor? {
        didSet {
            backgroundColor = background
        }
    }
    
    @IBInspectable var inset: CGFloat = 0 {
        didSet {
            contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        }
    }
    
}
