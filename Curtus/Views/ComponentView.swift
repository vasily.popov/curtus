//
//  ComponentView.swift
//  Curtus
//
//  Created by Vasily Popov on 12/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

@IBDesignable
final class ComponentView: UIView {
    
    @IBOutlet var componentLabel: UILabel!
    
    @IBInspectable var title: String? {
        didSet {
            componentLabel.text = title
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var background: UIColor? {
        didSet {
            backgroundColor = background
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        
        let bundle = Bundle.main
        let nib = UINib( nibName: String(describing:type(of:self)), bundle: bundle)
        nib.instantiate(withOwner: self, options: nil)
        addSubview(componentLabel)
        
        componentLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            componentLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            componentLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
 
    }
}
