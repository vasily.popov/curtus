//
//  UICollectionView.swift
//  Curtus
//
//  Created by Vasily Popov on 15/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

extension UICollectionViewCell: ReusableView {
}

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
}
