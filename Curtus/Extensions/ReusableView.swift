//
//  ReusableView.swift
//  Curtus
//
//  Created by Vasily Popov on 15/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

protocol ReusableView:class {
    static var defaultReuseIdentifier: String { get }
}


protocol NibLoadableView: class {
    static var nibName: String { get }
}


extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return self.nameOfClass
    }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return self.nameOfClass
    }
}
