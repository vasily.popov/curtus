//
//  UITableView.swift
//  Curtus
//
//  Created by Vasily Popov on 14/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

extension UITableViewCell: ReusableView {
}

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) {
        register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UITableViewCell>(_: T.Type) where T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
}
