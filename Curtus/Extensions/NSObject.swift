//
//  NSObject.swift
//  Curtus
//
//  Created by Vasily Popov on 15/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import Foundation

public extension NSObject {
    class var nameOfClass: String {
        return "\(self)"
    }
    
    var nameOfClass: String {
        return "\(self)"
    }
}
