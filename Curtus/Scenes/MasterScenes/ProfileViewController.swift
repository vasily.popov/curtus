//
//  ProfileViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 13/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

final class ProfileLifeCycleBehavior: ViewControllerLifecycleBehavior {
    
    private var initialColor : UIColor?
    
    func afterAppearing(_ viewController: UIViewController) {
        initialColor = viewController.view.backgroundColor
        viewController.view.backgroundColor = .black
        viewController.navigationController?.navigationBar.barStyle = .black
        
    }
    
    func beforeDisappearing(_ viewController: UIViewController) {
        viewController.view.backgroundColor = initialColor
        viewController.navigationController?.navigationBar.barStyle = .default
    }
}

class ProfileNavigationBarBehavior: ViewControllerLifecycleBehavior {
    
    private weak var target: StatusBarStyleBehaiour?
    
    init( _ target: StatusBarStyleBehaiour) {
        self.target = target
    }
    
    func beforeAppearing(_ viewController: UIViewController) {
        target?.statusBarStyle = .lightContent
        
    }
    
    func beforeDisappearing(_ viewController: UIViewController) {
        target?.statusBarStyle = .default
    }
}

class ProfileViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var behaviors : [ViewControllerLifecycleBehavior] = [ProfileLifeCycleBehavior()]
        
        if let statuBarBehaiour = self.splitViewController as? StatusBarStyleBehaiour {
            behaviors.append(ProfileNavigationBarBehavior(statuBarBehaiour))
        }
        addBehaviors(behaviors: behaviors)
    }
}
