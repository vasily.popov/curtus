//
//  FeedViewCell.swift
//  Curtus
//
//  Created by Vasily Popov on 14/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

class FeedViewCell: UITableViewCell {
    
    @IBOutlet weak var cellText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureModel(_ item: String) {
        self.cellText.text = item
    }
}

extension FeedViewCell : NibLoadableView {
    
}
