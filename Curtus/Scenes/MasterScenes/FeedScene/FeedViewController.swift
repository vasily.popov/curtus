//
//  FeedViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 14/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    private let elements = Service.tableTestDataSourceProvider
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(FeedViewCell.self)
    }
}

extension FeedViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.elements[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedViewCell.defaultReuseIdentifier, for: indexPath)
        if let cell = cell as? FeedViewCell {
            cell.configureModel(item)
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = UIStoryboard(name: "FeedScene", bundle: nil).instantiateViewController(withIdentifier: "SessionSummary")
        navigationController?.pushViewController(viewController, animated: true)
    }
}
