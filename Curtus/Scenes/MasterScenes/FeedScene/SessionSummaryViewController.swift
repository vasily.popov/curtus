//
//  FeedSecondViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 13/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

enum SegueIdentifiers : String {
    case removeFromStack
}

class SessionSummaryViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SegueIdentifiers.removeFromStack.rawValue {
            if var feedLast = segue.destination as? NavigationStackFiltered {
                feedLast.filter = { !($0 is SessionSummaryViewController) }
            }
        }
    }

}
