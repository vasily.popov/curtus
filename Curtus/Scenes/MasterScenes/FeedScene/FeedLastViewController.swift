//
//  FeedLastViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 13/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

protocol NavigationStackFiltered {
    var filter: ((UIViewController) -> Bool)? {get set}
}

class FeedLastViewController: UIViewController, NavigationStackFiltered {
    
    var filter: ((UIViewController) -> Bool)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let filter = filter, let newStack = self.navigationController?.viewControllers.filter(filter) {
            self.navigationController?.setViewControllers(newStack, animated: false)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func popToRoot(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
