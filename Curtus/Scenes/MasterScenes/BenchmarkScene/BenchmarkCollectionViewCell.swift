//
//  BenchmarkCollectionViewCell.swift
//  Curtus
//
//  Created by Vasily Popov on 15/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

class BenchmarkCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellText.text = "0"
        // Initialization code
    }

    func configureModel(_ value: Int, _ stopped: Bool) {
        cellText.text = String(value)
        innerView.backgroundColor = stopped ? .black :.purple
    }
}

extension BenchmarkCollectionViewCell : NibLoadableView {
    
}
