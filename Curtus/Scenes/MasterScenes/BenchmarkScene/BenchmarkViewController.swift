//
//  BenchmarkViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 13/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

typealias TimerFiredClosure = (IndexPath) -> Void


final class CountTimer {
    
    private var timer : Timer?
    private var counter : Int = 0
    var value: Int {
        return counter
    }
    
    var isStopped: Bool {
        return timer == nil
    }
    
    func start() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runTimed), userInfo: nil, repeats: true)
    }
    
    func invalidate() {
        stop()
        counter = 0
    }
    
    func toggle() {
        if timer != nil {
           stop()
        } else {
            start()
        }
    }
    
    private func stop() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc private func runTimed() {
        counter += 1
    }
    
}

final class BenchmarkLifeCycleBehavior: ViewControllerLifecycleBehavior {
    
    private let elements: [CountTimer]?
    private var reloadTimer : Timer?
    private weak var reloadedView : ReloadedCollectionView?
    
    init(_ elements: [CountTimer], reloadedView: ReloadedCollectionView) {
        self.elements = elements
        self.reloadedView = reloadedView
    }
    
    func afterAppearing(_ viewController: UIViewController) {
        elements?.forEach { $0.start() }
        reloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runTimed), userInfo: nil, repeats: true)
    }
    
    func beforeDisappearing(_ viewController: UIViewController) {
        elements?.forEach { $0.invalidate() }
        reloadTimer?.invalidate()
        reloadedView?.reloadCollectionView()
    }
    
    @objc private func runTimed() {
        reloadedView?.reloadCollectionView()
    }
    
}

protocol ReloadedCollectionView: class {
    func reloadCollectionView()
}

class BenchmarkViewController: UIViewController {

    private let elements = Service.collectionTestDataSourceProvider
    private var direction: UICollectionView.ScrollDirection = .vertical
    @IBOutlet weak var collection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBehaviors(behaviors: [BenchmarkLifeCycleBehavior(elements, reloadedView: self)])
        collection.register(BenchmarkCollectionViewCell.self)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadLayoutTapped))
    }
    
    @objc func reloadLayoutTapped() {
        
        direction = (direction == .vertical) ?.horizontal:.vertical
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = direction
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.itemSize = CGSize(width: 90, height: 90)
        collection.setCollectionViewLayout(layout, animated: true)
    }
}

extension BenchmarkViewController: ReloadedCollectionView {
    func reloadCollectionView() {
        collection.reloadData()
    }
}

extension BenchmarkViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elements.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.elements[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BenchmarkCollectionViewCell.defaultReuseIdentifier, for: indexPath)
        if let cell = cell as? BenchmarkCollectionViewCell {
            cell.configureModel(item.value, item.isStopped)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.elements[indexPath.row]
        item.toggle()
    }

}
