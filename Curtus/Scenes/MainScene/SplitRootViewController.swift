//
//  SplitRootViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 13/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

protocol StatusBarStyleBehaiour : class {
    var statusBarStyle : UIStatusBarStyle {get set}
}

class SplitRootViewController: UISplitViewController, StatusBarStyleBehaiour {
    var statusBarStyle: UIStatusBarStyle = .default
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
}

