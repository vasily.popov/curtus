//
//  DetailViewController.swift
//  Curtus
//
//  Created by Vasily Popov on 12/06/2019.
//  Copyright © 2019 Vasily Popov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let isPresented = navigationController?.isBeingPresented, !isPresented {
            navigationItem.leftBarButtonItems = nil
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

}
